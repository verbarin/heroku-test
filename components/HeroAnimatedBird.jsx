import React from 'react';

class Detecter extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            y: 0,
            scaleFactor: 1,
            opacity: 0.2,
            opacityBird: 1
        }

        this.container = React.createRef()
        this.handler = this.handler.bind(this)
    }

    componentDidMount() {
        window.addEventListener('scroll', (e) => this.handler(e));
    }

    handler() {
        if(this.container !== undefined && this.container.current != null) {
            this.setState({
                y: window.pageYOffset,
                scaleFactor: 1 + window.pageYOffset / 500,
                opacity: 0.2 + window.pageYOffset / 500,
                opacityBird: 1 - window.pageYOffset / 250
            })
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handler)
    }

    render() {
        return (
            <React.Fragment>
                <div ref={this.container} className="bird_wrapper" style={{
                    transform: `translateY(-${this.state.y / 10}%) scale(${this.state.scaleFactor})`,
                    opacity: this.state.opacity
                }}>
                    <div className="spark_container hide-sm" style={{
                        opacity: this.state.opacityBird
                    }}>
                    </div>
                    
                    <img src="/assets/svg/bird_1.svg" alt="" className="bird_1" />
                </div>
            </React.Fragment>
        )
    }
}

export default Detecter