import axios from "axios";

const LOG_URL = 'https://upfinity-proxy.herokuapp.com/cl/logs/';

const logEvent = (id, log_name, data, token, lang) => {
  console.log(lang);
  return new Promise((resolve, reject) => {
    axios.post(LOG_URL + token, {
        type_id: id,
        type_name: log_name,
        data: data,
        timestamp: new Date().toISOString(),
        lang: lang
    }, {
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    })
    .then(response => {
        resolve(true);
    })
    .catch(e => {
        console.log(e)
    })
  })
}

export default logEvent;